<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
                exclude-result-prefixes="ent requtil">

    <xsl:output method="text"/>

    <xsl:template match="/">
        <xsl:variable name="CountryCode" select="ent:HotelsRequest/ent:Code"/>
        <xsl:variable name="CityName" select="ent:HotelsRequest/ent:CityName"/>
        <xsl:variable name="CodeLang" select="ent:HotelsRequest/ent:CodeLang"/>
        <xsl:variable name="PartOfName" select="ent:HotelsRequest/ent:PartOfName"/>
        <xsl:variable name="Category" select="ent:HotelsRequest/ent:Category_ID"/>
        <xsl:text>select lang.code as lang_code, hot.id as hotel_id, hotname.name as hot_name, hot.code as hcode, hot.url as url, hot.check_in as hot_in, hot.check_out as hot_out,
            hot.latitude as hot_lat, hot.longitude as hot_long, hot.fax as fax, hot.email as email, lcoun.name as country_name,
            city.code as city_code, country.code as coun_code, hot.code as hot_code, lcity.name as city_name
			from gpt_hotel hot join gpt_location city on city.id=hot.city_id
			join gpt_location country on country.id=hot.country_id
	        join gpt_location_name lcoun on lcoun.location_id=city.parent
			join gpt_location_name lcity on lcity.location_id=city.id
			join gpt_hotel_name hotname on hotname.hotel_id=hot.id
	        join gpt_language lang on lcoun.lang_id=lang.id
			join gpt_language langcity on lcity.lang_id=langcity.id
			join gpt_language langhotel on hotname.lang_id=langhotel.id</xsl:text>
        <xsl:text> where country.code="</xsl:text>
        <xsl:value-of select="$CountryCode"/>
        <xsl:text>"</xsl:text>
        <xsl:if test="string-length($CityName) &gt; 0">
        <xsl:text> and lcity.name="</xsl:text>
        <xsl:value-of select="$CityName"/>
        <xsl:text>"</xsl:text>
        </xsl:if>
        <xsl:text> and lang.code="</xsl:text>
        <xsl:value-of select="$CodeLang"/>
        <xsl:text>" and langhotel.code="</xsl:text>
        <xsl:value-of select="$CodeLang"/>
        <xsl:text>" and langcity.code="</xsl:text>
        <xsl:value-of select="$CodeLang"/>
        <xsl:text>" and hotname.name like '%</xsl:text>
        <xsl:value-of select="$PartOfName"/>
        <xsl:text>%'</xsl:text>
        <xsl:if test="string-length($Category) &gt; 0">
            <xsl:text>and hot.category_id="</xsl:text>
            <xsl:value-of select="$Category"/>
            <xsl:text>"</xsl:text>
        </xsl:if>


    </xsl:template>


</xsl:stylesheet>