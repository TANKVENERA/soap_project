<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
                xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
                exclude-result-prefixes="ent sutil queryutil">

    <xsl:output method="xml"/>

    <xsl:template match="/">


        <xsl:variable name="CountryName" select="Result/Entry/country_name"/>
        <xsl:variable name="CountryCode" select="Result/Entry/coun_code"/>
        <xsl:variable name="Lang" select="Result/Entry/lang_code"/>
        <ent:HotelsResponse>
            <ent:Country Lang="{$Lang}" Name="{$CountryName}" Code="{$CountryCode}">
                <xsl:for-each select="Result/Entry">
                    <xsl:variable name="CityName" select="city_name"/>
                    <xsl:variable name="CityCode" select="city_code"/>
                    <xsl:variable name="HotelName" select="hot_name"/>
                    <xsl:variable name="HotelCode" select="hot_code"/>
                    <xsl:variable name="HotelId" select="hotel_id"/>
                    <ent:City Lang="{$Lang}" Name="{$CityName}" Code="{$CityCode}">
                        <ent:Hotel Lang="{$Lang}" Name="{$HotelName}" Code="{$HotelCode}">
                            <ent:Fax>
                                <xsl:value-of select="fax"/>
                            </ent:Fax>
                            <ent:Email>
                                <xsl:value-of select="email"/>
                            </ent:Email>
                            <ent:URL>
                                <xsl:value-of select="url"/>
                            </ent:URL>
                            <ent:Check_In>
                                <xsl:value-of select="hot_in"/>
                            </ent:Check_In>
                            <ent:Check_Out>
                                <xsl:value-of select="hot_out"/>
                            </ent:Check_Out>
                            <ent:Latitude>
                                <xsl:value-of select="hot_lat"/>
                            </ent:Latitude>
                            <ent:Longitude>
                                <xsl:value-of select="hot_long"/>
                            </ent:Longitude>
                            <ent:Images>
                                <xsl:variable name="queryURL">
                                    <xsl:text>select url from gpt_hotel_images where hotel_id=</xsl:text>
                                </xsl:variable>
                                <xsl:variable name="currentUrl"
                                              select="queryutil:subQuery(concat($queryURL, $HotelId))"/>
                                <xsl:for-each select="$currentUrl/Entry">
                                    <xsl:variable name="queryType">
                                <xsl:text>select  imgtype.type_name from gpt_hotel_images img
                                join gpt_hotel_image_type imgtype on imgtype.id=img.type_id where img.url="</xsl:text>
                                        <xsl:value-of select="."/>
                                        <xsl:text>"</xsl:text>
                                    </xsl:variable>
                                    <xsl:variable name="type" select="queryutil:subQuery(concat($queryType,''))"/>
                                    <ent:Image URL="{.}" Type="{$type}"></ent:Image>
                                </xsl:for-each>
                            </ent:Images>
                        </ent:Hotel>
                    </ent:City>
                </xsl:for-each>
            </ent:Country>
        </ent:HotelsResponse>
    </xsl:template>


</xsl:stylesheet>