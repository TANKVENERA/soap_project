CREATE DATABASE IF NOT EXISTS `service_static` DEFAULT CHARACTER SET utf8;
use `service_static`;


DROP TABLE IF EXISTS `cities`;
DROP TABLE IF EXISTS `countries`;
DROP TABLE IF EXISTS `currencies`;

create table `currencies` (
  `id` serial not null,
  `code` varchar(3) not null,
  primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `countries` (
  `country_id` serial not null,
  `name` varchar(300) not null,
  `currency_id` bigint unsigned not null,
  foreign key (`currency_id`) references currencies(`id`),
  primary key (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `cities` (
  `city_id` serial not null,
  `country_id` bigint unsigned not null,
  `name` varchar(300) not null,
  `active` tinyint(1) not null,
  foreign key (`country_id`) references countries(`country_id`),
  primary key (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into `currencies`(code) values('EUR');
SET @eur = LAST_INSERT_ID();
insert into `currencies`(code) values('BYN');
SET @byn = LAST_INSERT_ID();
insert into `currencies`(code) values('pln');
SET @pln = LAST_INSERT_ID();

insert into `countries`(name, currency_id) values('Spain', @eur);
SET @country = LAST_INSERT_ID();
insert into `cities`(name, country_id, active) values('Madrid', @country, 1);
insert into `cities`(name, country_id, active) values('Barcelona', @country, 1);

insert into `countries`(name, currency_id) values('Belarus', @byn);
SET @country = LAST_INSERT_ID();
insert into `cities`(name, country_id, active) values('Minsk', @country, 1);
insert into `cities`(name, country_id, active) values('Mozyr', @country, 1);

insert into `countries`(name, currency_id) values('Poland', @pln);
SET @country = LAST_INSERT_ID();
insert into `cities`(name, country_id, active) values('Warsaw', @country, 1);
insert into `cities`(name, country_id, active) values('Krakow', @country, 1);

insert into countries (name, currency_id) values ('Fake', 1);


CREATE TABLE `gpt_hotel` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(32) NOT NULL,
  `CITY_ID` bigint(20) NOT NULL,
  `country_id` bigint(10) DEFAULT NULL,
  `CHAIN_ID` bigint(20) DEFAULT NULL,
  `PHONE` varchar(100) NOT NULL,
  `FAX` varchar(50) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `CHECK_IN` time DEFAULT NULL,
  `CHECK_OUT` time DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `ZIP` varchar(50) DEFAULT NULL,
  `latitude` decimal(10,3) DEFAULT NULL,
  `longitude` decimal(10,3) DEFAULT NULL,
  `ACTIVE` bit(1) DEFAULT NULL,
  `default_image` bigint(10) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `CATEGORY_ID` enum('HOTEL','HOSTEL','APARTMENTS') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CITY_ID` (`CITY_ID`),
  CONSTRAINT `gpt_hotel_ibfk_1` FOREIGN KEY (`CITY_ID`) REFERENCES `gpt_location` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;





CREATE TABLE `gpt_hotel_image_type` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;



CREATE TABLE `gpt_hotel_images` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HOTEL_ID` bigint(20) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `URL` varchar(600) NOT NULL,
  `THUMNAIL_URL` varchar(600) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `HOTEL_ID` (`HOTEL_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_2` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_hotel_image_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;



CREATE TABLE `gpt_hotel_name` (
  `HOTEL_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `NAME` varchar(500) NOT NULL,
  PRIMARY KEY (`HOTEL_ID`,`LANG_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `gpt_language` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



CREATE TABLE `gpt_location` (
  `ID` bigint(32) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `PARENT` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `latitude` decimal(10,3) DEFAULT NULL,
  `longitude` decimal(10,3) DEFAULT NULL,
  `TIME_ZONE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_location_ibfk_1` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_location_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


CREATE TABLE `gpt_location_name` (
  `LOCATION_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`LOCATION_ID`,`LANG_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_location_name_ibfk_1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `gpt_location` (`ID`),
  CONSTRAINT `gpt_location_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `gpt_location_type` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;