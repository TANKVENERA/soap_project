-- MySQL dump 10.13  Distrib 5.7.18, for Win64 (x86_64)
--
-- Host: localhost    Database: gp
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `city_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` bigint(20) unsigned NOT NULL,
  `name` varchar(300) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city_id` (`city_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,1,'Madrid',1),(2,1,'Barcelona',1),(3,2,'Minsk',1),(4,2,'Mozyr',1),(5,3,'Warsaw',1),(6,3,'Krakow',1),(7,5,'St_Petersburg',1),(8,5,'Rostov',1);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `country_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `currency_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_id` (`country_id`),
  KEY `currency_id` (`currency_id`),
  CONSTRAINT `countries_ibfk_1` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Spain',1),(2,'Belarus',2),(3,'Poland',3),(4,'Fake',1),(5,'Russia',4);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (1,'EUR'),(2,'BYN'),(3,'pln'),(4,'RUR');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel`
--

DROP TABLE IF EXISTS `gpt_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(32) NOT NULL,
  `CITY_ID` bigint(20) NOT NULL,
  `country_id` bigint(10) DEFAULT NULL,
  `CHAIN_ID` bigint(20) DEFAULT NULL,
  `PHONE` varchar(100) NOT NULL,
  `FAX` varchar(50) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `CHECK_IN` time DEFAULT NULL,
  `CHECK_OUT` time DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `ZIP` varchar(50) DEFAULT NULL,
  `latitude` decimal(10,3) DEFAULT NULL,
  `longitude` decimal(10,3) DEFAULT NULL,
  `ACTIVE` bit(1) DEFAULT NULL,
  `default_image` bigint(10) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `CATEGORY_ID` enum('HOTEL','HOSTEL','APARTMENTS') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CITY_ID` (`CITY_ID`),
  CONSTRAINT `gpt_hotel_ibfk_1` FOREIGN KEY (`CITY_ID`) REFERENCES `gpt_location` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel`
--

LOCK TABLES `gpt_hotel` WRITE;
/*!40000 ALTER TABLE `gpt_hotel` DISABLE KEYS */;
INSERT INTO `gpt_hotel` VALUES (1,'0001',4,3,NULL,'111-000','333-999','/33/SS','12:00:00','10:00:00','@Orbita',NULL,53.540,27.290,NULL,NULL,NULL,'HOTEL'),(2,'0002',5,2,NULL,'11-11','22-000','/ST/ST','10:00:00','10:00:00','@Nevski',NULL,59.550,55.480,'',NULL,NULL,'HOSTEL'),(3,'0003',4,3,NULL,'11-11','22-22','/W/W','13:00:00','14:00:00','@Hampton_Hilton',NULL,53.530,27.320,'',NULL,NULL,'HOSTEL'),(4,'0004',10,3,NULL,'999-000','123-123','/gomel','12:00:00','10:30:00','@Gomel_Hotel',NULL,33.000,22.000,'',NULL,'2017-11-07 10:53:20','HOSTEL');
/*!40000 ALTER TABLE `gpt_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_image_type`
--

DROP TABLE IF EXISTS `gpt_hotel_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_image_type` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_image_type`
--

LOCK TABLES `gpt_hotel_image_type` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_image_type` DISABLE KEYS */;
INSERT INTO `gpt_hotel_image_type` VALUES (1,'GENERAL'),(2,'ROOM'),(3,'LOBBY'),(4,'PARKING');
/*!40000 ALTER TABLE `gpt_hotel_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_images`
--

DROP TABLE IF EXISTS `gpt_hotel_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_images` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HOTEL_ID` bigint(20) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `URL` varchar(600) NOT NULL,
  `THUMNAIL_URL` varchar(600) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `HOTEL_ID` (`HOTEL_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_2` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_hotel_image_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_images`
--

LOCK TABLES `gpt_hotel_images` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_images` DISABLE KEYS */;
INSERT INTO `gpt_hotel_images` VALUES (1,1,1,'/ORBITA/MINSK/GENERAL','/$$$'),(2,2,1,'/HOTEL_NEVSKI/PITER/GENERAL','/@@@'),(3,3,3,'/HILTON/MINSK/Lobby','/***'),(4,3,1,'/HILTON/MINSK/GENERAL','/$$$'),(5,3,2,'/HILTON/MINSK/ROOM','$$$'),(7,3,4,'/HILTON/MINSK/PARKING','$$$'),(8,4,2,'/HOTEL/GOMEl/ROOM','/###');
/*!40000 ALTER TABLE `gpt_hotel_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_name`
--

DROP TABLE IF EXISTS `gpt_hotel_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_name` (
  `HOTEL_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `NAME` varchar(500) NOT NULL,
  PRIMARY KEY (`HOTEL_ID`,`LANG_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_name`
--

LOCK TABLES `gpt_hotel_name` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_name` DISABLE KEYS */;
INSERT INTO `gpt_hotel_name` VALUES (1,1,'ORBITA'),(2,1,'HOTEL_NEVSKI'),(3,1,'HAMPTON_BY_HILTON'),(3,2,'ХЭМПТОН-ХИЛТОН'),(4,1,'HOTEL_GOMEL');
/*!40000 ALTER TABLE `gpt_hotel_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_language`
--

DROP TABLE IF EXISTS `gpt_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_language` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_language`
--

LOCK TABLES `gpt_language` WRITE;
/*!40000 ALTER TABLE `gpt_language` DISABLE KEYS */;
INSERT INTO `gpt_language` VALUES (1,'EN','ENGLISH'),(2,'RU','RUSSIAN'),(3,'BY','BELARUSSIAN');
/*!40000 ALTER TABLE `gpt_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location`
--

DROP TABLE IF EXISTS `gpt_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location` (
  `ID` bigint(32) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `PARENT` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `latitude` decimal(10,3) DEFAULT NULL,
  `longitude` decimal(10,3) DEFAULT NULL,
  `TIME_ZONE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_location_ibfk_1` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_location_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location`
--

LOCK TABLES `gpt_location` WRITE;
/*!40000 ALTER TABLE `gpt_location` DISABLE KEYS */;
INSERT INTO `gpt_location` VALUES (1,'0001',1,0,'',0.000,0.000,NULL),(2,'0002',2,1,'',60.000,100.000,NULL),(3,'0003',2,1,'',53.000,28.000,NULL),(4,'0004',3,3,'',54.000,27.600,NULL),(5,'0005',3,2,'',59.800,30.300,NULL),(9,'0009',2,1,'',38.000,97.000,NULL),(10,'0006',3,3,'',55.000,123.000,NULL);
/*!40000 ALTER TABLE `gpt_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_name`
--

DROP TABLE IF EXISTS `gpt_location_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_name` (
  `LOCATION_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`LOCATION_ID`,`LANG_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_location_name_ibfk_1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `gpt_location` (`ID`),
  CONSTRAINT `gpt_location_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_name`
--

LOCK TABLES `gpt_location_name` WRITE;
/*!40000 ALTER TABLE `gpt_location_name` DISABLE KEYS */;
INSERT INTO `gpt_location_name` VALUES (1,1,'ROOT'),(2,1,'RUSSIA'),(2,2,'РОССИЯ'),(3,1,'BELARUS'),(3,2,'БЕЛАРУСЬ'),(4,1,'MINSK'),(4,2,'МИНСК'),(5,1,'ST_PITERBURG'),(9,2,'США'),(10,1,'Gomel');
/*!40000 ALTER TABLE `gpt_location_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_type`
--

DROP TABLE IF EXISTS `gpt_location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_type` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_type`
--

LOCK TABLES `gpt_location_type` WRITE;
/*!40000 ALTER TABLE `gpt_location_type` DISABLE KEYS */;
INSERT INTO `gpt_location_type` VALUES (1,'ROOT'),(2,'COUNTRY'),(3,'CITY');
/*!40000 ALTER TABLE `gpt_location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-07 17:56:33
