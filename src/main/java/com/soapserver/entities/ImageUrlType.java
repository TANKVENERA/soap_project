
package com.soapserver.entities;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Image_UrlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Image_UrlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;sequence>
 *           &lt;element name="Image_Url" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="99"/>
 *           &lt;element name="Image_Type" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="99"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Image_UrlType", propOrder = {
    "imageUrl",
    "imageType"
})
public class ImageUrlType {

    @XmlElement(name = "Image_Url", required = true)
    protected List<String> imageUrl;
    @XmlElement(name = "Image_Type", required = true)
    protected List<String> imageType;

    /**
     * Gets the value of the imageUrl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the imageUrl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImageUrl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getImageUrl() {
        if (imageUrl == null) {
            imageUrl = new ArrayList<String>();
        }
        return this.imageUrl;
    }

    /**
     * Gets the value of the imageType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the imageType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImageType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getImageType() {
        if (imageType == null) {
            imageType = new ArrayList<String>();
        }
        return this.imageType;
    }

}
