package com.soapserver.entities;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.5.1
 * 2017-11-07T21:29:47.321+03:00
 * Generated source version: 2.5.1
 * 
 */
@WebService(targetNamespace = "http://soapserver.com/entities", name = "ServicePort")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ServicePort {

    @WebMethod(action = "hotel_data")
    @WebResult(name = "HotelsResponse", targetNamespace = "http://soapserver.com/entities", partName = "HotelsResponse")
    public HotelsResponse processHotelsRequest(
        @WebParam(partName = "HotelsRequest", name = "HotelsRequest", targetNamespace = "http://soapserver.com/entities")
        HotelsRequest hotelsRequest
    ) throws FaultMessage;

    @WebMethod(action = "country_data")
    @WebResult(name = "CountryResponse", targetNamespace = "http://soapserver.com/entities", partName = "CountryResponse")
    public CountryResponse processCountryRequest(
        @WebParam(partName = "CountryRequest", name = "CountryRequest", targetNamespace = "http://soapserver.com/entities")
        CountryRequest countryRequest
    ) throws FaultMessage;
}
