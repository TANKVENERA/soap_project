
package com.soapserver.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodeLang" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartOfName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Category_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "code",
    "cityName",
    "codeLang",
    "partOfName",
    "categoryID"
})
@XmlRootElement(name = "HotelsRequest")
public class HotelsRequest {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "CityName", required = true)
    protected String cityName;
    @XmlElement(name = "CodeLang", required = true)
    protected String codeLang;
    @XmlElement(name = "PartOfName", required = true)
    protected String partOfName;
    @XmlElement(name = "Category_ID", required = true)
    protected String categoryID;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the cityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Sets the value of the cityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityName(String value) {
        this.cityName = value;
    }

    /**
     * Gets the value of the codeLang property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeLang() {
        return codeLang;
    }

    /**
     * Sets the value of the codeLang property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeLang(String value) {
        this.codeLang = value;
    }

    /**
     * Gets the value of the partOfName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartOfName() {
        return partOfName;
    }

    /**
     * Sets the value of the partOfName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartOfName(String value) {
        this.partOfName = value;
    }

    /**
     * Gets the value of the categoryID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryID() {
        return categoryID;
    }

    /**
     * Sets the value of the categoryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryID(String value) {
        this.categoryID = value;
    }

}
