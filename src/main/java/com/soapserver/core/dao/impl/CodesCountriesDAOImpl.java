package com.soapserver.core.dao.impl;

import com.soapserver.core.dao.CodesDAO;
import com.soapserver.core.dao.JdbcExt;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Mina on 07.11.2017.
 */
public class CodesCountriesDAOImpl extends JdbcExt implements CodesDAO {

    private static final String CODES = "select code from gpt_location";

    @Override
    public List<String> getCodesNames() {
        return getJdbcTemplate().query(CODES, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int index) throws SQLException {
                return rs.getString("Code");
            }

        });
    }
}
