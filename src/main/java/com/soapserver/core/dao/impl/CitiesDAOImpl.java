package com.soapserver.core.dao.impl;

import com.soapserver.core.dao.CitiesDAO;
import com.soapserver.core.dao.JdbcExt;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Mina on 07.11.2017.
 */
public class CitiesDAOImpl extends JdbcExt implements CitiesDAO {

    private static final String CITIES = "select name from gpt_location_name";

    @Override
    public List<String> getCitiesNames() {
        return getJdbcTemplate().query(CITIES, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int index) throws SQLException {
                return rs.getString("name");
            }

        });
    }


}
