package com.soapserver.core.dao;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Mina on 07.11.2017.
 */
public interface CitiesDAO {


    @Transactional(
            readOnly = true,
            rollbackFor = Throwable.class
    )
    List<String> getCitiesNames();
}
