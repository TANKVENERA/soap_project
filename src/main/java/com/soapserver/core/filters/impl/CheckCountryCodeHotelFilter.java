package com.soapserver.core.filters.impl;

import com.soapserver.core.dao.CodesDAO;
import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;
import com.soapserver.entities.HotelsResponse;

import java.util.List;

/**
 * Created by Mina on 07.11.2017.
 */
public class CheckCountryCodeHotelFilter implements PreFilter<HotelsRequest> {

    private CodesDAO codesDAO;


    @Override
    public boolean isApplicable(final HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        if (request.getCode().isEmpty()) {
            throw new ServiceException("Country code is required");
        }
       else if (request.getCode() != null) {
            final List<String> codes = codesDAO.getCodesNames();
            if (!codes.contains(request.getCode())) {
                throw new ServiceException("Requested code doesn't exist");
            }
        }

    }

    public void setCodesDAO(CodesDAO codesDAO) {
        this.codesDAO = codesDAO;
    }
}
