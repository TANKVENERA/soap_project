package com.soapserver.core.filters.impl;

import com.soapserver.core.dao.CitiesDAO;
import com.soapserver.core.dao.CodesDAO;
import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;

import java.util.List;

/**
 * Created by Mina on 07.11.2017.
 */
public class CheckCitiesHotelFilter implements PreFilter<HotelsRequest> {

    private  CitiesDAO citiesDAO;

    @Override
    public boolean isApplicable(final  HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        if (!request.getCityName().isEmpty()) {
            final List<String> cities = citiesDAO.getCitiesNames();
            if (!cities.stream().anyMatch(request.getCityName()::equalsIgnoreCase)) {
                throw new ServiceException("Requested city doesn't exist");
            }
        }
    }

    public void setCititesDAO(CitiesDAO cititesDAO) {
        this.citiesDAO = cititesDAO;
    }
}
