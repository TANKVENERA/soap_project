package com.soapserver.core.filters.impl;

import com.soapserver.core.filters.PostFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.*;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Mina on 07.11.2017.
 */
public class LowerCaseUrlFilter implements PostFilter<HotelsRequest, HotelsResponse> {

    @Override
    public boolean isApplicable(HotelsRequest request, HotelsResponse response) {
        if (response.getCountry().isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public void postProcess(HotelsRequest request, HotelsResponse response) throws ServiceException {

        List<Image> images = new ArrayList<>();

        for (CountryTypeHotel countryTypeHotel : response.getCountry()) {
            for (CityType citiesType : countryTypeHotel.getCity()) {
                images.addAll(citiesType.getHotel().getImages().getImage());
            }
        }

        for (Image image : images) {
            String url = image.getURL().toLowerCase();
            image.setURL(url);
        }
    }
}

