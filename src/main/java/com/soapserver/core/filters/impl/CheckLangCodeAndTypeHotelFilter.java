package com.soapserver.core.filters.impl;

import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;

/**
 * Created by Mina on 07.11.2017.
 */
public class CheckLangCodeAndTypeHotelFilter implements PreFilter<HotelsRequest> {


    @Override
    public boolean isApplicable(final  HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {

            if (!request.getCodeLang().equals("en") && !request.getCodeLang().equals("ru")) {
                throw new ServiceException("Only 'ru' or 'en' code language are allowed");
            }

            if (!request.getCategoryID().isEmpty()) {
                if (request.getCategoryID().compareToIgnoreCase("hotel") != 0 &&
                        request.getCategoryID().compareToIgnoreCase("hostel") != 0 &&
                        request.getCategoryID().compareToIgnoreCase("apartments") != 0) {
                    throw new ServiceException("Only 'hotel','hostel' or 'apartments' are allowed");
                }
            }
    }


    @Override
    public boolean equals(Object obj) {
        if (obj==null) return false;
        if (obj==this) return true;
        return false;
    }
}
